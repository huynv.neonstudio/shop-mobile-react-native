
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  Button,
  TouchableOpacity,
  ActivityIndicator,
  AsyncStorage,
  Alert
} from "react-native";
import api from "./api";
import { NavigationContainer,useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

const LoginScreen = () => {
  const navigation = useNavigation()
  const [username, setusername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setloading] = useState(false);
  
  const login = async () =>{
    setloading(true)
   const resultLogin = await api.login({
      username : username,
      password : password
    })
    setloading(false)
    if(resultLogin.isSuccess)
    {
    await AsyncStorage.setItem("",JSON.stringify(resultLogin))
    Alert.alert("Thông báo","Đăng nhập thành công")
    navigation.navigate("Home")
  }else{
    Alert.alert("Lỗi","Đăng nhập thất bại")
  }
  }
  return (
    <View style={styles.container}>
     
      <Image style={styles.image} source={require("./assets/transparent-shopping-online-icon-digital-marketing-icon-phone-5dcf004cf3eed3.8422330315738471169992.jpg")} />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Tên đăng nhập"
          placeholderTextColor="#003f5c"
          onChangeText={(username) => setusername(username)}
        />
      </View>
 
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Mật khẩu"
          placeholderTextColor="#003f5c"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
 
      <TouchableOpacity>
        <Text style={styles.forgot_button}>Quên mật khẩu?</Text>
      </TouchableOpacity>
 
      <TouchableOpacity onPress={login} style={styles.loginBtn}>
      {loading==true&&<ActivityIndicator color={"#fff"} />}
        <Text style={styles.loginText}>Đăng nhập</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
 
  image: {
    height : 150,
    width: 150,
    marginBottom: 40,
  },
 
  inputView: {
    backgroundColor: "#FFC0CB",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,
 
    alignItems: "center",
  },
 
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },
 
  forgot_button: {
    height: 30,
    marginBottom: 30,
  },
 
  loginBtn: {
    flexDirection:'row',
    width: "80%",
    borderRadius: 25,
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 40,
    backgroundColor: "#FF1493",
  },
});


const Stack = createNativeStackNavigator();


function HomeScreen() {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Home Screen</Text>
    </View>
  );
}
function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;